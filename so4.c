
//cmd: gcc -o test test.c -lpthread
//./test




#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>


int contor=0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *f(void *arg){

	int *id=(int*)arg;

	while(1){

		pthread_mutex_lock(&mutex);

		if(contor>100){
			//Contorul depaseste valoarea ceruta, deci trebuie eliberat lacatul

			pthread_mutex_unlock(&mutex);
			pthread_exit(NULL);
		}

		else{//sunt in zona de incrementari inca, unde thread urile incrementeaza pe rand

				if((contor<30)&&(*id==0)){
					printf("Thread %d, contor %d \n ", *id, contor);
					printf("\n");
					contor++;
				}

				if((contor>=30 && contor <=89)&&((contor%3+1)==*id))){

					printf("Thread %d, contor %d \n", *id, contor);
					printf("\n");
					contor++;
				}

				if((contor>=90)&&(*id==4)){
					printf("Thread %d, contor %d \n", *id, contor);
					printf("\n");
					contor++;
				}
			pthread_mutex_unlock(&mutex);

		}
	

	}


}



int main(int argc, char argv[]){

printf("Start\n");
pthread_t t0, t1, t2, t3, t4;
pthread_t id[]={0,1,2,3,4};

if(pthread_create(&t0, NULL, &f, (void*)&id[0])!=0){
	printf("Eroare");
	exit(1);
}

if(pthread_create(&t1, NULL, &f, (void*)&id[1])!=0){
	printf("Eroare");
	exit(2);
}

if(pthread_create(&t2, NULL, &f, (void*)&id[2])!=0){
	printf("Eroare");
	exit(3);
}

if(pthread_create(&t3, NULL, &f, (void*)&id[3])!=0){
	printf("Eroare");
	exit(4);
}

if(pthread_create(&t4, NULL, &f, (void*)&id[4])!=0){
	printf("Eroare");
	exit(5);
}

pthread_join(&t0,NULL);
pthread_join(&t1,NULL);
pthread_join(&t2,NULL);
pthread_join(&t3,NULL);
pthread_join(&t4,NULL);

return 0;

}
