#include<stdio.h>
#include<stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

#define bufsize 500
//in rezultat final se afla numarul total de rezultate
int rezultat_final;


//functia pentru thread si pentru lucrul sau cu fisierul
void* functie(char *fis){
	
	int x;
	int sursa;
	int rezultat=0;
	
	if ((sursa = open(fis, O_RDONLY)) <0){
		perror("Eroare la deschiderea fisierului");
	
	}
	
	//citim din fisier
	//cat timp se citeste din fisier
	while ( x = read(fis,bufsize, sizeof(bufsize))){
		if ( x<0 ) {
			perror("Eroare");
		}
		 

	}

	//Cod pentru gasirea instruciunilor exit si incrementarea rezultatului
	close(sursa);
	printf("thread-ul fisierului %s returneaza %d", &fis, rezultat);
	rezultat_final=rezult_final+rezultat;


}



int main(int argc, char **argv[])
{
	int nr =argc;
	//vectorul v de caractere va avea aceeasi lungime cu numarul de argumente
	char v[nr];
	//atata timp cat avem cel putin un fisier dat ca argument, incepand de la pozitia 0
	while (argc >= 0){
		
		//punem in v numele fisierelor
		for(int i = 1; i <= nr; i++){
			if (argv[i] != NULL ){
				v[i-1]=argv[i];
			}
		}

	}




	int pid1, pid2, pid3, pid4, pid5;
	int status1, status2, status3, status4, status5;
	
	printf("Proces1/INIT: PID: %d  PID-parinte:  %d\n", (int*)getpid(), (int*)getppid());
	pid2=fork(); 

	if (pid2 > 0){
		//ne aflam in parinte, adica in p1
		waitpid(pid2, &status2, NULL);
		pid3=fork();

		if (pid3 > 0){
			//ne aflam in parintele lui p3, adica tot in p1
			waitpid(pid3, &status3, NULL);
			pid4=fork();
			
			if (pid4 > 0){
				//ne aflam in parintele lui p4, adica tot in p1
				waitpid(pid4, &status4, NULL);	
			}
			else if (pid4 == 0){
				//ne aflam chiar in p4
				printf("Proces4: PID: %d  PID-parinte:  %d\n", (int*)getpid(), (int*)getppid());
			}


		}
		else if (pid3 == 0){
			//ne aflam chiar in p3
			printf("Proces3: PID: %d  PID-parinte:  %d\n", (int*)getpid(), (int*)getppid());
			
			pid5=fork();

			if (pid5 > 0){
				//ne aflam in parintele lui p5, adica in p3
				waitpid(pid5, &status5, NULL);
			}
			else if (pid5 == 0){
				//ne aflam chiar in pid5
				printf("Proces5: PID: %d  PID-parinte:  %d\n", (int*)getpid(), (int*)getppid());

			}	
			

			//COD PENTRU THREAD
			
			//pentru fiecare fisier se creeaza un thread
			for(int i=0; i<nr; i++){
				
				pthread_t thread;
				//in v[i] se afla numele fisierului, deci il dam ca argument functiei
				if(pthread_create(&thread, NULL, functie, &v[i] )==1){
					perror("Eroare de executie");
					exit(0);
				}
				
				pthread_join(thread, NULL);
			}



		}

	}	

	else if (pid2 == 0){
		//ne aflam chiar in p2

		printf("Proces2: PID: %d  PID-parinte:  %d\n", (int*)getpid(), (int*)getppid());

	}
	

	return 0;
}
